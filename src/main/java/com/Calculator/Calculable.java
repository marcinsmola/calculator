package com.Calculator;

/**
 * Created by marcin on 18.05.17.
 */
public interface Calculable {
    void add(int a,int b);
    void minus(int a,int b);
    void divide(int a,int b);
    void multiply(int a,int b);
}
