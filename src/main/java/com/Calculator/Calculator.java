package com.Calculator;

import java.io.IOException;

/**
 * Created by marcin on 18.05.17.
 */
public class Calculator implements Calculable {
    @Override
    public void add(int a, int b) {
        int sum = a + b;
        System.out.println(sum);
    }
    @Override
    public void multiply(int a, int b) {
        System.out.println(a*b);
    }
    @Override
    public void minus(int a, int b) {
        System.out.println(a-b);
    }

    @Override
    public void divide(int a, int b) {
        int divide = 0;
        try {
            divide = a / b;
        } catch (ArithmeticException e) {
            System.out.println("You can't divide by 0!");
        }

    }


}
